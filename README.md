# stargz demo

This repo documents the steps required to deployment a multi cloud distributed
setup relying on replicated Harbor registries and Argo for workflow submission.

This was the same setup used for a Kubecon 2020 talk.

## Overview

We rely on two cluster per region:
* one to host the Harbor registry
* one to host the workloads and configured with the stargz snapshotter

In the actual demo europe-west4-a and us-central1-c were used.

## Cluster Creation

A similar command can be used to create both clusters. For Harbor either we
need a large node (16 cores) or a multi node clusters (4x 4 cores).
```
gcloud beta container clusters create --cluster-version 1.17.12-gke.1501 \
    --machine-type e2-standard-16 --image-type UBUNTU_CONTAINERD \
    --disk-type pd-standard --disk-size 100 \
    --num-nodes 1 --no-enable-autoupgrade --no-enable-autorepair \
    --zone europe-west4-a \
    harbor-west4-a
```
```
gcloud beta container clusters create --cluster-version 1.17.12-gke.1501 \
    --machine-type e2-standard-4 --image-type UBUNTU_CONTAINERD \
    --disk-type pd-standard --disk-size 100 \
    --num-nodes 5 --no-enable-autoupgrade --no-enable-autorepair \
    --zone europe-west4-a \
    stargz-west4-001
```

## Harbor Setup

We rely on helm for the Harbor setup.

The GCS bucket used for artifact storage must be created in advance: it should
be made regional as we have one per deployment.
```
helm install harbor harbor/harbor --version 1.5.0 --values values-demo.yaml
    \ --set persistence.imageChartStorage.gcs.bucket=stargz-harbor-west4
```

Fetch the service external ip.
```
kubectl get svc
NAME                       TYPE           CLUSTER-IP     EXTERNAL-IP     PORT(S)             AGE
harbor                     LoadBalancer   10.3.246.177   34.70.249.205   80:31888/TCP        59s
```

Apply the default configuration for all registries. This includes setting up
a proxy cache for both the CERN registry and docker.io and a replication rule
also from the CERN registry.
```
cd config/harbor/json
./create.sh HARBOR-EXTERNAL-IP
```

Harbor should be available at: http://HARBOR-EXTERNAL-IP, with user/pass
admin:12345678.

## Stargz Cluster Configuration

We need to upgrade containerd and configure the stargz
snapshotter - which currently requires containerd >=1.4 and by default
UBUNTU_CONTAINERD deployments on GKE have 1.2.

The preparation script below will also:
* setup containerd/stargz-snapshotter so it can access the registries
  insecurely
* setup the Argo workflow controller for later submission

The required script is made available in this repository.
```
cd config/stargz
vim stargz-cm.yaml
( add a resolver.host section for each harbor registry setup )
( add a registry.mirrors section for each harbor registry setup )
```
```
./prepare.sh europe-west4-a stargz-west4-001
```

It should be run for each cluster to be used for workload submission.

## Workflow Submission

A simple script is given with sample submissions for each cluster. Edit
appropriately replace the registry IP with the ones from your setup in the
image parameter.
```
cd config/argo
./submit-argo.sh
```

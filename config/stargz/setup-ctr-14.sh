#!/bin/bash
set -x
set +e

rm -rf usr/ etc/ opt/ cri-containerd*
wget --quiet https://github.com/containerd/containerd/releases/download/v1.4.1/cri-containerd-cni-1.4.1-linux-amd64.tar.gz
tar zxvf cri-containerd-cni-1.4.1-linux-amd64.tar.gz
 
sudo systemctl stop kubelet || true
sudo crictl ps | grep Runnin | awk '{print $1}' | xargs sudo crictl rm -f || true
sudo systemctl stop containerd || true
sudo ps auxw | grep containerd | awk '{print $2}' | xargs sudo kill -9 || true
sudo cp usr/local/bin/* /usr/bin
sudo rm -rf /opt/cni && sudo cp -R opt/cni /opt/
sudo systemctl start containerd
sleep 10
sudo systemctl start kubelet


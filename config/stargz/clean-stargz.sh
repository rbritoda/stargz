#!/bin/bash
set -x
set +e

sudo systemctl stop kubelet || true
sudo crictl ps | grep Runnin | awk '{print $1}' | xargs sudo crictl rm -f || true
sudo systemctl stop stargz-snapshotter || true
sudo systemctl stop containerd || true
sudo ps auxw | grep containerd | awk '{print $2}' | xargs sudo kill -9 || true
for i in `seq 1 1000`; do sudo umount /var/lib/containerd-stargz-grpc/snapshotter/snapshots/${i}/fs; done
sudo rm -rf /var/lib/containerd-stargz-grpc/
sudo rm -rf /var/lib/containerd
sudo systemctl start containerd
sudo systemctl start stargz-snapshotter
sleep 15
sudo systemctl start kubelet

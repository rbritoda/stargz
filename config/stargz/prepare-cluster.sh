#!/bin/bash
set +x

ZONE=$1
CLUSTER=$2

gcloud compute config-ssh
gcloud container clusters get-credentials --zone $ZONE $CLUSTER
for n in $(kubectl get node -o custom-columns='DATA:metadata.name' | grep gke); do
	scp -o StrictHostKeyChecking=no setup-ctr-14.sh ${n}.${ZONE}.nimble-valve-236407:~/
	ssh -o StrictHostKeyChecking=no ${n}.${ZONE}.nimble-valve-236407 ~/setup-ctr-14.sh
done

helm install stargz-snapshotter ./stargz-snapshotter/charts/stargz-snapshotter
kubectl apply -f stargz-cm.yaml
for n in $(kubectl get node -o custom-columns='DATA:metadata.name' | grep gke); do
	kubectl label node $n stargz=true
done

kubectl create namespace argo
kubectl apply -n argo -f https://raw.githubusercontent.com/argoproj/argo/v2.11.6/manifests/install.yaml
kubectl create clusterrolebinding argo-cluster-admin-binding --clusterrole=cluster-admin --serviceaccount=argo:default
kubectl apply -f argo-workflow-controller-cm.yaml
kubectl patch svc argo-server -n argo -p '{"spec": {"type": "LoadBalancer"}}'
kubectl -n argo get svc 


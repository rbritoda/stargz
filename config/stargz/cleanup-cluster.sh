#!/bin/bash
set +x

ZONE=$1
CLUSTER=$2

gcloud container clusters get-credentials --zone $ZONE $CLUSTER
for n in $(kubectl get node -o custom-columns='DATA:metadata.name' | grep gke); do
	scp -o StrictHostKeyChecking=no clean-stargz.sh ${n}.${ZONE}.nimble-valve-236407:~/
	ssh -o StrictHostKeyChecking=no ${n}.${ZONE}.nimble-valve-236407 ~/clean-stargz.sh
done

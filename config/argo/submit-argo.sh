#!/bin/bash
set +x
kubectl config use-context gke_nimble-valve-236407_europe-west4-a_stargz-west4-001
argo submit -n argo -p image=34.90.240.0:80/stargz/python:3.9.0-1gb-urandom-esgz -p paralleljobs=20 athena-workflow.yaml

kubectl config use-context gke_nimble-valve-236407_us-central1-c_stargz-central1-001
argo submit -n argo -p image=34.122.253.98:80/stargz/python:3.9.0-1gb-urandom -p paralleljobs=20 athena-workflow.yaml

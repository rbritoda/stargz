#!/bin/bash
set +x
IP=$1
curl  -L -k -X POST "http://${IP}/api/v2.0/registries" -u 'admin:12345678' -H 'Content-Type: application/json' -d@registry-docker.io.json
curl  -L -k -X POST "http://${IP}/api/v2.0/registries" -u 'admin:12345678' -H 'Content-Type: application/json' -d@registry-cernharbor.json
curl  -L -k -X POST "http://${IP}/api/v2.0/projects" -u 'admin:12345678' -H 'Content-Type: application/json' -d@project-docker.io.json
curl  -L -k -X POST "http://${IP}/api/v2.0/projects" -u 'admin:12345678' -H 'Content-Type: application/json' -d@project-cerncache.json
curl  -L -k -X POST "http://${IP}/api/v2.0/replication/policies" -u 'admin:12345678' -H 'Content-Type: application/json' -d@replication-cern.json
